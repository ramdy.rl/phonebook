from agenda.models import Phonebook

def valid_number(number):
    number = str(number)
    if len(number) != 10 and len(number) != 13 and len(number) != 14:
        return False
    else:
        if len(number) == 10:
            prefix = check_prefix(number[0], 10)
            operator = check_operator(number[1:3])
            one_digit = check_one_digit(number[3])
            last_digits = check_last_digits(number[4:10])
        elif len(number) == 13:
            prefix = check_prefix(number[0:4], 13)
            operator = check_operator(number[4:6])
            one_digit = check_one_digit(number[6])
            last_digits = check_last_digits(number[7:14])
        else:
            prefix = check_prefix(number[0:5], 14)
            operator = check_operator(number[5:7])
            one_digit = check_one_digit(number[7])
            last_digits = check_last_digits(number[8:15])

        return prefix == operator == one_digit == last_digits == True   

def check_prefix(number, lenght):
    if lenght == 10:
        return str(number) == '0'
    elif lenght == 13:
        return str(number) == '+359'
    elif lenght == 14:
        return str(number) == '00359'
    else:
        return False

def check_operator(number):
    return (int(number) >= 87 and int(number) <= 89)

def check_one_digit(number):
    return (int(number) >= 2 and int(number) <= 9)

def check_last_digits(number):
    import re
    match = re.search(r'\d+', str(number))
    return len(match.group()) == 6

def import_line(line):
    line = str(line).split(' ')
    if len(line) != 2:
        return False
    number = str(line[1]).split('\n')[0]
    validator = valid_number(number)
    if validator:
        contact = Phonebook()
        contact.name = line[0]
        contact.number = line[1]
        contact.save()
        return contact

    return False