from django.db import models

# Create your models here.

class Phonebook(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)

    class Meta:
        ordering = ['name']

class Outgoing(models.Model):
    call = models.ForeignKey(Phonebook)
    date = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-date']