from django.http.response import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic.base import View
from agenda.models import Phonebook, Outgoing
from agenda.utils import import_line, valid_number


def contacts(request):
    list_contacts = Phonebook.objects.all()
    context = {'view': 'contacts', 'contacts': list_contacts}
    return render(request,'contacts.html', context)

def outgoing(request):
    calls = Outgoing.objects.all()[:5]
    context = {'view': 'outgoing', 'calls': calls}
    return render(request,'outgoing.html', context)

class Importing(View):

    def get(self, request):
        context = {'view': 'import'}
        return render(request,'import.html', context)

    def post(self, request):
        f = request.FILES['file']
        with open('media/phonebook.txt', 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)

        f = open('media/phonebook.txt', 'r')
        list_contacts = []
        for line in f.readlines():
            contact = import_line(line)
            if contact:
                list_contacts.append(contact)
        context = {'view': 'import', 'contacts': list_contacts, 'imported': len(list_contacts)}
        return render(request,'import.html', context)

class AddContact(View):

    def get(self, request):
        context = {'view': 'contacts'}
        return render(request,'contact_add.html', context)

    def post(self, request):
        name = request.POST.get('name')
        number = request.POST.get('number')
        validator = valid_number(number)
        if validator:
            contact = Phonebook(name=name, number=number)
            contact.save()
            return redirect('/contacts/')
        else:
            msg = 'This number is not a valid Bulgarian phone number'
            context = {'view': 'contacts', 'errors': True, 'msg': msg}
            return render(request,'contact_add.html', context)

def search_contacts(request):
    query = request.GET.get('query')
    list_contacts = Phonebook.objects.filter(name__istartswith=query)
    context = {'contacts': list_contacts}
    return render(request,'search.html', context)


def remove_contact(request):
    id = request.POST.get('id')
    contact = Phonebook.objects.get(pk=id)
    contact.delete()
    return HttpResponse()

def make_call(request):
    id = request.GET.get('id')
    contact = Phonebook.objects.get(pk=id)
    call = Outgoing(call=contact)
    call.save()
    context = {'contact': contact}
    return render(request,'calling.html', context)

