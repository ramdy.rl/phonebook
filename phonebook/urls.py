"""phonebook URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from agenda import views

urlpatterns = [
    url(r'^$',views.contacts, name='index'),
    url(r'^contacts/',views.contacts, name='contacts'),
    url(r'^outgoing/',views.outgoing, name='outgoing'),
    url(r'^import/',views.Importing.as_view(), name='import'),
    url(r'^contact/add/',views.AddContact.as_view(), name='contact'),
    url(r'^remove-contact/',views.remove_contact, name='remove'),
    url(r'^make-call/',views.make_call, name='call'),
    url(r'^search-contacts/',views.search_contacts, name='search'),

    url(r'^admin/', admin.site.urls),
]
