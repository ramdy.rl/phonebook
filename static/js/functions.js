$(function () {
    $('#search_contact').keyup(function(){
        $.get('/search-contacts/',
            {'query': $(this).val()},
            function(data){
                $('#table_area').html(data)
            }
        )
    })
})

function removeContact(id) {
    $('#'+id).hide('slow', function(){
        $(this).remove();
        $.post('/remove-contact/',
            {'csrfmiddlewaretoken':$("input[name=csrfmiddlewaretoken]").val(), 'id':id}
        )
    })
}

function make_a_call(id) {
    $.get('/make-call/',
        {'id': id},
        function(data){
            $('#tocall').html(data).show('slow').hide(4000);

        }
    )
}